package com.LUMA.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;

import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.LUMA.baseclass.BaseClass;
import com.aventstack.extentreports.Status;

import utility.Log;

public class PlaceOrderPage extends BaseClass {

	@FindBy(xpath = "//input[@name=\"street[0]\"]")

	WebElement street;

	@FindBy(xpath = "//input[@name=\"city\"]")

	WebElement city;

	@FindBy(xpath = "//select[@name=\"region_id\"]")

	WebElement state;

	@FindBy(xpath = "//input[@name=\"postcode\"]")

	WebElement postalCode;

	@FindBy(xpath = "//select[@name=\"country_id\"]")

	WebElement country;

	@FindBy(xpath = "//input[@name=\"telephone\"]")

	WebElement phoneNumber;

	@FindBy(xpath = "//*[@class=\"counter-number\"]")

	WebElement number2;

	@FindBy(xpath = "//input[@name=\"billing-address-same-as-shipping\"]")

	WebElement checkbox;

	public PlaceOrderPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	public void orderPage() throws Exception {

		et = er.createTest("placing order_new user");

		wait = new WebDriverWait(driver, 20);

		wait.until(ExpectedConditions.visibilityOf(number2));

		et.log(Status.INFO, "placing order");

		// Log.info("Clicking on cart button");

		cart.click();

		// Log.info("Clicking on proceed to checkout button");

		checkout.click();

		// Log.info("Entering the street");

		et.log(Status.INFO, "entering the address");
		street.sendKeys("abcd near 4th Lane");

		// Log.info("Entering the city");

		city.sendKeys("ranchi");

		// Log.info("entering the postal code");

		postalCode.sendKeys("12345-6789");

		// Log.info("Selecting the country from dropdown");

		Select abc = new Select(country);

		abc.selectByVisibleText("United States");

		// Log.info("Selecting state from dropdown");

		Select se = new Select(state);

		se.selectByVisibleText("Alaska");

		// Log.info("Entering the phone number");

		phoneNumber.sendKeys("9835234544");

		// Log.info("Selecting one of the shipping methods.");

		shippingMethod.click();

		// Log.info("Clicking on next button");

		et.pass("address entered sucessfully");

		next.click();

		// Log.info("Selecting the checkbox.");

		wait.until(ExpectedConditions.visibilityOf(checkbox));

		checkbox.click();

		// Log.info("Checkbox is selected");

		checkbox.isSelected();

		// Log.info("Clciking on place order button");

		order.click();

		// Log.info("Taking screenshot of confirmation message after order has been
		// placed");

		TakesScreenshot ts = (TakesScreenshot) driver;

		File file = ts.getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(file, new File(
				"C:\\\\Users\\\\mubeen khan\\\\eclipse-workspace\\\\LUMA_application\\\\Screenshots\\\\screen_2.jpeg"));

		et.log(Status.PASS, "placed order sucessfully");
	}

}
