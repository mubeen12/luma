package com.LUMA.pages;
 
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
 
import com.LUMA.baseclass.BaseClass;
import com.aventstack.extentreports.Status;

import utility.Log;
 
public class AddingItemsPage extends BaseClass {
 
@FindBy(xpath="//img[@alt=\"Radiant Tee\"]")

WebElement Item1;
 
@FindBy(id="option-label-size-143-item-168")

WebElement Item1Size;
 
@FindBy(id="option-label-color-93-item-50")

WebElement Item1Colour;
 
@FindBy(id="qty")

WebElement Quantity;
 
@FindBy(id="product-addtocart-button")

WebElement addToCart;

 
@FindBy(xpath="//*[@id=\"maincontent\"]/div[1]/div[2]/div/div/div")

WebElement Confirmation;
 
 
 
//Next Item
 
@FindBy(id="ui-id-6")

WebElement Gear;
 
@FindBy(xpath="//*[text()=\"Bags\"]")

WebElement Bags;
 
@FindBy(xpath="//*[@id=\"maincontent\"]/div[3]/div[1]/div[4]/ol/li[6]/div/a/span/span/img")

WebElement greenBag ;
 
//Next Item

@FindBy(id="ui-id-5")

WebElement Mens;
 
@FindBy(id="ui-id-17")

WebElement Tops;
 
@FindBy(id="ui-id-20")

WebElement Hoodies;
 
@FindBy(xpath="//*[@id=\"maincontent\"]/div[3]/div[1]/div[4]/ol/li[2]/div/div/strong/a")

WebElement sweatshirt;
 
@FindBy(id="option-label-size-143-item-170")

WebElement sweatShirtSize;
 
@FindBy(id="option-label-color-93-item-58")

WebElement sweatShirtColour;
 
 
 
public AddingItemsPage(WebDriver driver) {

	PageFactory.initElements(driver,this);

}
 
 
public void AddItems() {
	
	et=er.createTest("AddingItemsPage");

	//Procedure of adding first item

	et.log(Status.INFO, "adding the first item");
	
//	Log.info("Clicking on first item");

	Item1.click();

	//Log.info("selecting size of the cloth we want.");

	Item1Size.click();

//	Log.info("Selecting colour of cloth we want");

	Item1Colour.click();

//	Log.info("Clearing the quantity textbox");

	Quantity.clear();

//	Log.info("Entering the quantity of cloth we want.");

	Quantity.sendKeys("2");

//	Log.info("Clicking on add to cart button");

	addToCart.click();

//	Log.info("Checking whether conirmation message of item successfully added is displayed or not.");

	Confirmation.isDisplayed();


	et.log(Status.PASS, "first item added sucessfully");
	
	
		Actions act = new Actions(driver);

	//	Log.info("Performing mouse hovering action on gears items");

		act.moveToElement(Gear).build().perform();

		//Procedure of adding 2nd item

	//	Log.info("Clicking on Bags");

		et.log(Status.INFO, "adding the second item");
		
		Bags.click();

	//	Log.info("Selecting this particular bag from list of bags.");

		greenBag.click();

	//	Log.info("Clicking on add to cart button.");

		addToCart.click();

		et.log(Status.PASS, "second item added sucessfully");
		
	//	Log.info("Performing mouse hovering action on mens items");

		act.moveToElement(Mens).build().perform();

	//	Log.info("Performing mouse hovering action on tops items");

		act.moveToElement(Tops).build().perform();

		//Procedure of adding third item

	//	Log.info("Clicking on hoodies");

		et.log(Status.INFO, "adding the third item");
		
		Hoodies.click();

	//	Log.info("Selecting a particular sweatshirt from list of sweatshirts.");

		sweatshirt.click();

//		Log.info("Choosing size of shirt we want");

		sweatShirtSize.click();

	//	Log.info("Choosing colour of shirt we want");

		sweatShirtColour.click();

	//	Log.info("Clearing the quantity textbox.");

		Quantity.clear();

	//	Log.info("Entering the quntity of shirt we want.");

		Quantity.sendKeys("4");

	//	Log.info("Clicking on add to cart button");

		addToCart.click();
		
	//	Log.info("Checking whwether confirmation of adding shirt to cart is displayed or not.");

		Confirmation.isDisplayed();
		
		et.log(Status.PASS, "third item added sucessfully");

		et.log(Status.PASS, "all the items were added sucessfully");
		
	}	

}
 
 