package com.LUMA.pages;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
 
import com.LUMA.baseclass.BaseClass;
import com.aventstack.extentreports.Status;

import utility.Log;
 
public class LoginPage extends BaseClass{
boolean flag;
 
@FindBy(xpath="(//*[text()=\"Customer Login\"])[2]")
WebElement h1;
 
@FindBy(xpath="/html/body/div[2]/header/div[1]/div/ul/li[2]/a")
WebElement signIn;
 
@FindBy(id="email")
WebElement email2;
 
@FindBy(id="pass")
WebElement password2;
 
@FindBy(id="send2")
WebElement signInBtn;
 
 
public LoginPage(WebDriver driver) {
	PageFactory.initElements(driver,this);
}
 
public void validatelogin(String uname,String pwd,String uname1,String pwd1) {
	
	et= er.createTest("Logging in");
	
	et.log(Status.INFO, "signing in");
	//Log.info("Clicking on Sign In Button.");
	signIn.click();
	//Log.info("Storing the boolean value of h1 in flag");
	flag = h1.isDisplayed();
//	Log.info("Checking the if value of flag is true or not");
	Assert.assertTrue(flag);

//	Log.info("Validated h1 tag successfully");
 

//	Log.info("Entering the username");
	email2.sendKeys(uname);
	//Log.info("Entering the password");
	password2.sendKeys(pwd);
//	Log.info("Clicking on Sign In Button after entering the credentials");
	signInBtn.click();
	
	email2.clear();
	email2.sendKeys(uname1);
	password2.clear();
	password2.sendKeys(pwd1);
	
	signInBtn.click();
	et.log(Status.PASS, "user signed in sucessfully");
}
}

