package com.LUMA.pages;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.LUMA.baseclass.BaseClass;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import utility.Log;

public class PlaceOrderForRegisteredUserPage extends BaseClass {

	@FindBy(xpath = "(//div[@role=\"tab\"])[2]")

	WebElement showItems;

	@FindBy(xpath = "//*[@class=\"counter-number\"]")

	WebElement number;

	@FindBy(xpath = "//*[@class=\"button action continue primary\"]")

	WebElement Next;

	@FindBy(xpath = "//a[@class=\"order-number\"]")

	WebElement orderNumber;

	public PlaceOrderForRegisteredUserPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	public void existingUser() throws Exception {

		et = er.createTest("placing order_for_registered_user");

		wait = new WebDriverWait(driver, 20);

		wait.until(ExpectedConditions.visibilityOf(number));

		et.log(Status.INFO, "placing order");

		// Log.info("Clicking on cart button");

		cart.click();

//		Log.info("Clicking on procced to checkout");

		checkout.click();

		// Log.info("Selecting one of the shipping methods.");

		shippingMethod.click();

		// Log.info("Clicking on arrow button to see what are the items user we have
		// added");

		showItems.click();

		// Log.info("Clicking on next button");

		Next.click();

		// wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath("//button[@title=\\\"Place
		// Order\\\"]"))));

		Thread.sleep(30000);
		// Log.info("Clicking on place order button");

		order.click();

		wait.until(ExpectedConditions.visibilityOf(orderNumber));

		// Log.info("Clciking on order number");

		orderNumber.click();

		// Screenshot of particular section

		// Log.info("Taking the screenshot of confirmation message after placing
		// order");

		// WebElement section =
		// driver.findElement(By.xpath("//*[@id=\"maincontent\"]/div[2]/div[1]"));

		/*
		 * File source1 = section.getScreenshotAs(OutputType.FILE);
		 * 
		 * File target1 = new
		 * File("C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\Screenshots\\screen_1.jpeg"
		 * );
		 * 
		 * FileUtils.copyFile(source1,target1);
		 */

		takescreenshot();

		String absolutePath = new File(
				"C:\\\\Users\\\\mubeen khan\\\\eclipse-workspace\\\\LUMA_application\\\\test-output\\\\ExReports\\\\qwerty.jpg")
				.getAbsolutePath();

		et.pass("details", MediaEntityBuilder.createScreenCaptureFromPath(absolutePath).build());
		
		et.addScreenCaptureFromPath(absolutePath);

		et.log(Status.PASS, " order placed  sucessfully");

	}

	public void takescreenshot() throws Exception {

		TakesScreenshot ts = (TakesScreenshot) driver;
		File src = ts.getScreenshotAs(OutputType.FILE);
		File trg = new File(
				"C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\test-output\\ExReports\\qwerty.jpg");
		FileUtils.copyFile(src, trg);
	}

}
