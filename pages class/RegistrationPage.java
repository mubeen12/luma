package com.LUMA.pages;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

import org.testng.Assert;

import com.LUMA.baseclass.BaseClass;
import com.aventstack.extentreports.Status;

import utility.Log;

public class RegistrationPage extends BaseClass {

	@FindBy(xpath = "(//*[text()=\"Create an Account\"])[1]")

	WebElement createAnAccount;

	@FindBy(xpath = "//input[@id=\"firstname\"]")

	WebElement firstName;

	@FindBy(xpath = "//input[@id=\"lastname\"]")

	WebElement lastName;

	@FindBy(xpath = "//input[@name=\"email\"]")

	WebElement email;

	@FindBy(xpath = "(//input[@name=\"password\"])[1]")

	WebElement password;

	@FindBy(xpath = "//input[@name=\"password_confirmation\"]")

	WebElement confirmPassword;

	@FindBy(xpath = "(//button[@type=\"submit\"])[2]")

	WebElement createAccount;

	@FindBy(xpath = "//*[text()=\"Thank you for registering with Main Website Store.\"]")

	WebElement message;

	@FindBy(xpath = "(//button[@type=\"button\"])[1]")

	WebElement welcomeBtn;

	@FindBy(xpath = "/html/body/div[2]/header/div[1]/div/ul/li[2]/div/ul/li[3]/a")

	WebElement signOut;

	public RegistrationPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	public void validateRegisterURL() {

		// et.log(Status.INFO, "validating the url");
		// Log.info("This is my expected URL");

		String expectedURL = "https://magento.softwaretestingboard.com/";

		// Log.info("Getting the current URL.");

		String actualURL = driver.getCurrentUrl();

		// Log.info("Checking if expectedURL and actualURL matches or not.");

		Assert.assertEquals(expectedURL, actualURL);

		// et.pass("url matched");
	}

	public void validateRegister() throws Exception {

		et = er.createTest("registration");

		et.log(Status.INFO, "creating a new user account");
		//// Log.info("Clicking on Create an Account button");

		createAnAccount.click();

		FileInputStream fis = new FileInputStream(
				"C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\ExcelData\\Excel_operations.xlsx");

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheetAt(0);

		int rows = sheet.getLastRowNum();

		short columns = sheet.getRow(1).getLastCellNum();

		for (int r = 1; r <= rows; r++) {

			XSSFRow row = sheet.getRow(r);

			// Log.info("Entering the first name.");

			et.log(Status.INFO, "entering the mandatory details");
			firstName.sendKeys(row.getCell(0).getStringCellValue());

			// Log.info("Entering the last name");

			lastName.sendKeys(row.getCell(1).getStringCellValue());

			// Log.info("Entering the email");

			email.sendKeys(row.getCell(2).getStringCellValue());

			// Log.info("Entering the password");

			password.sendKeys(row.getCell(3).getStringCellValue());

			// Log.info("Entering the password again");

			confirmPassword.sendKeys(row.getCell(4).getStringCellValue());

			et.log(Status.PASS, "details were entered successfully");
			// Log.info("Clicking on Create Account button");

			createAccount.click();

			et.pass("created new user successfully");

			//
			et.log(Status.INFO, "Checking whether thanyou message is displayed or not ");

			// Log.info("Checking whether thanyou message is displayed or not");

			message.isDisplayed();

			// Log.info("Click on welcome button");

			welcomeBtn.click();

			// Log.info("Clicking on Sign out button");

			et.log(Status.PASS, "Clicking on Sign out button");
			signOut.click();

			er.flush();
		}	
	}
}
