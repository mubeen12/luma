package com.LUMA.baseclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.LUMA.pages.AddingItemsPage;
import com.LUMA.pages.LoginPage;
import com.LUMA.pages.PlaceOrderForRegisteredUserPage;
import com.LUMA.pages.PlaceOrderPage;
import com.LUMA.pages.RegistrationPage;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class BaseClass {

	public static RegistrationPage rp;
	public static LoginPage lp;
	public static PlaceOrderPage po;
	public static AddingItemsPage add;
	public static PlaceOrderForRegisteredUserPage poru;
	public static WebDriver driver;

	@FindBy(xpath = "/html/body/div[2]/header/div[2]/div[1]/a")
	public static WebElement cart;
	@FindBy(id = "top-cart-btn-checkout")
	public static WebElement checkout;
	@FindBy(xpath = "//*[text()=\"Table Rate\"]")
	public static WebElement shippingMethod;
	@FindBy(xpath = "//*[@id=\"shipping-method-buttons-container\"]/div/button")
	public static WebElement next;
	@FindBy(xpath = "//button[@title=\"Place Order\"]")
	public static WebElement order;
	public static WebDriverWait wait;
	public static ExtentSparkReporter esr;
	public static ExtentReports er;
	public static ExtentTest et;

	/*
	 * @BeforeSuite public void method() { DOMConfigurator.configure("Log_4j.xml");
	 * Log.info("This is before Suite"); }
	 */

	@Parameters({ "browser" })
	@BeforeClass
	public void setUp(String browser) {

		if (browser.equalsIgnoreCase("Chrome")) {

			String path = ".//test-output//ExReports//Report_m1.html";
			esr = new ExtentSparkReporter(path);
			esr.config().setDocumentTitle("Automation Report");
			esr.config().setReportName("LUMA Automation");
			esr.config().setTheme(Theme.DARK);
			er = new ExtentReports();
			er.attachReporter(esr);
			er.setSystemInfo("HostName", "lp-503122");
			er.setSystemInfo("Username", "mubeen khan");
			er.setSystemInfo("OS", "Windows");
			er.setSystemInfo("Window", "Windows-11");
			er.setSystemInfo("Environment", "QA");
			er.setSystemInfo("Date", "13/12/2023");

			/*
			 * System.setProperty("webdriver.edge.driver",
			 * "C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\drivers\\msedgedriver.exe"
			 * ); driver = new EdgeDriver();
			 */

			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\drivers\\chromedriver.exe");

			driver = new ChromeDriver();

			driver.get("https://magento.softwaretestingboard.com/");
			et = er.createTest("browser__");
			et.pass("Navigated to breowser successfully");
			driver.manage().window().maximize();
			et.pass("Browser window maximised");
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			rp = new RegistrationPage(driver);
			lp = new LoginPage(driver);
			add = new AddingItemsPage(driver);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			po = new PlaceOrderPage(driver);
			poru = new PlaceOrderForRegisteredUserPage(driver);

		} else {
			String path = ".//test-output//ExReports//Report_a1.html";
			esr = new ExtentSparkReporter(path);
			esr.config().setDocumentTitle("Automation Report");
			esr.config().setReportName("LUMA Automation");
			esr.config().setTheme(Theme.DARK);
			er = new ExtentReports();
			er.attachReporter(esr);
			er.setSystemInfo("HostName", "lp-503122");
			er.setSystemInfo("Username", "mubeen khan");
			er.setSystemInfo("OS", "Windows");
			er.setSystemInfo("Window", "Windows-11");
			er.setSystemInfo("Environment", "QA");
			er.setSystemInfo("Date", "13/12/2023");

			et = er.createTest("myfirsttest", "searching for automation");

			System.setProperty("webdriver.edge.driver",
					"C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\drivers\\msedgedriver.exe");
			driver = new EdgeDriver();

			/*
			 * System.setProperty("webdriver.chrome.driver",
			 * "C:\\Users\\mubeen khan\\eclipse-workspace\\LUMA_application\\drivers\\chromedriver.exe"
			 * );
			 * 
			 * driver= new ChromeDriver();
			 */

			driver.get("https://magento.softwaretestingboard.com/");
			et = er.createTest("browser__");
			et.pass("Navigated to breowser successfully");
			driver.manage().window().maximize();
			et.pass("Browser window maximised");
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			rp = new RegistrationPage(driver);
			lp = new LoginPage(driver);
			add = new AddingItemsPage(driver);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			po = new PlaceOrderPage(driver);
			poru = new PlaceOrderForRegisteredUserPage(driver);

		}
	}

	@AfterSuite
	public void tearDown() {
		// Log.info("Closing the browser.");

		er.flush();
		driver.close();

	}

}
