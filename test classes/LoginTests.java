package com.LUMA.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.LUMA.baseclass.BaseClass;

public class LoginTests extends BaseClass {
	/*
	 * @BeforeMethod public void beforemethod() { et = er.createTest("Logging in");
	 * }
	 */

	@Parameters({ "uname", "pwd" ,"uname1","pwd1" })
	@Test(dataProvider = "testdata")
	public void loginTest(String uname, String pwd, String uname1, String pwd1) {

		lp.validatelogin(uname, pwd,uname1, pwd1);
		//Log.info("Logged in successfully!");
		//et.log(Status.PASS, "Logged in successfully");
	}

	@DataProvider(name = "testdata")
	public static Object[][] testDataProvider() {
		Object[][] login_data = new Object[2][2];

		
		  login_data[0][0]="Admin"; 
		  login_data[0][1]="admin123";
		 
		login_data[1][0] = "mahak112@gmail.com";
		login_data[1][1] = "samba@123";
		return login_data;
	}

 // @AfterMethod public void aftermethod() { er.flush(); } 
  }
 