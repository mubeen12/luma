package com.LUMA.tests;
 
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.LUMA.baseclass.BaseClass;
import com.aventstack.extentreports.Status;
 
public class PlaceOrderForRegisteredUserTest extends BaseClass{
 
	LoginTests lt = new LoginTests();

	/*
	 * @BeforeMethod public void beforemethod() {
	 * et=er.createTest("PLacing order for registered user"); }
	 */
	@Test(dataProvider="testdata")
	public void placingOrderForRegisteredUser(String uname, String pwd,String uname1, String pwd1) throws Exception {
		lt.loginTest(uname,pwd,uname1,pwd1);
		poru.existingUser();
		et.log(Status.INFO, "placing order");
		//Log.info("Order placed successfully");
		et.log(Status.PASS,"Orders were placed successfully");
	}
	
	  @AfterMethod public void aftermethod() { er.flush(); }
	 
}